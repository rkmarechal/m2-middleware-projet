package controler;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import model.client.Client;
import model.entity.PariElement;
import view.View;

/*
 * Controleur repr�sentant la logique de la table de jeux.
 */
public class ControlerComponentSubViewTableGame implements ActionListener {

	// Composant du controleur "ControlerComponentSubViewTableGame"
	View v;
	Client c;
	// Tableau de bool�en pour savoir si le bouton a �t� cliqu�.
	Boolean isClicked[] = { false, false, false, false, false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, false, false, false, false, false, false, false, false, false,
			false, false, false, false, false, false, false, false };
	String[] pari = { "10", "20", "50", "100", "500", "1000" };

	// Constructeur du controleur ControlerComponentSubViewTableGame.
	public ControlerComponentSubViewTableGame(View p_v, Client p_c) {
		this.v = p_v;
		this.c = p_c;
	}

	// M�thode d'�coute sur le contr�le : actions effectu�es en appuyant sur les
	// JButtons.
	@SuppressWarnings("static-access")
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// Ecoute sur le bouton "connexion".
		if (arg0.getSource() == v.getComponentGame().getTableGame().getCase1()) {
			// Appuies sur le bouton "case n�1".
			// On v�rifie l'�tat du bouton.
			if (isClicked[0] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase1().setBackground(Color.RED);
				isClicked[0] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(1, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase1().setBackground(null);
				isClicked[0] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(1);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase2()) {
			// Appuies sur le bouton "case n�2".
			// On v�rifie l'�tat du bouton.
			if (isClicked[1] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase2().setBackground(Color.RED);
				isClicked[1] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(2, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase2().setBackground(null);
				isClicked[1] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(2);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase3()) {
			// Appuies sur le bouton "case n�3".
			// On v�rifie l'�tat du bouton.
			if (isClicked[2] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase3().setBackground(Color.RED);
				isClicked[2] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(3, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase3().setBackground(null);
				isClicked[2] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(3);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase4()) {
			// Appuies sur le bouton "case n�4".
			// On v�rifie l'�tat du bouton.
			if (isClicked[3] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase4().setBackground(Color.RED);
				isClicked[3] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(4, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase4().setBackground(null);
				isClicked[3] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(4);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase5()) {
			// Appuies sur le bouton "case n�5".
			// On v�rifie l'�tat du bouton.
			if (isClicked[4] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase5().setBackground(Color.RED);
				isClicked[4] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(5, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase5().setBackground(null);
				isClicked[4] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(5);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase6()) {
			// Appuies sur le bouton "case n�6".
			// On v�rifie l'�tat du bouton.
			if (isClicked[5] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase6().setBackground(Color.RED);
				isClicked[5] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(6, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase6().setBackground(null);
				isClicked[5] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(6);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase7()) {
			// Appuies sur le bouton "case n�7".
			// On v�rifie l'�tat du bouton.
			if (isClicked[6] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase7().setBackground(Color.RED);
				isClicked[6] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(7, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase7().setBackground(null);
				isClicked[6] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(7);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase8()) {
			// Appuies sur le bouton "case n�8".
			// On v�rifie l'�tat du bouton.
			if (isClicked[7] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase8().setBackground(Color.RED);
				isClicked[7] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(8, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase8().setBackground(null);
				isClicked[7] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(8);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase9()) {
			// Appuies sur le bouton "case n�9".
			// On v�rifie l'�tat du bouton.
			if (isClicked[8] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase9().setBackground(Color.RED);
				isClicked[8] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(9, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase9().setBackground(null);
				isClicked[8] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(9);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase10()) {
			// Appuies sur le bouton "case n�10".
			// On v�rifie l'�tat du bouton.
			if (isClicked[9] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase10().setBackground(Color.RED);
				isClicked[9] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(10, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase10().setBackground(null);
				isClicked[9] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(10);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase11()) {
			// Appuies sur le bouton "case n�11".
			// On v�rifie l'�tat du bouton.
			if (isClicked[10] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase11().setBackground(Color.RED);
				isClicked[10] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(11, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase11().setBackground(null);
				isClicked[10] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(11);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase12()) {
			// Appuies sur le bouton "case n�12".
			// On v�rifie l'�tat du bouton.
			if (isClicked[11] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase12().setBackground(Color.RED);
				isClicked[11] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(12, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase12().setBackground(null);
				isClicked[11] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(12);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase13()) {
			// Appuies sur le bouton "case n�13".
			// On v�rifie l'�tat du bouton.
			if (isClicked[12] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase13().setBackground(Color.RED);
				isClicked[12] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(13, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase13().setBackground(null);
				isClicked[12] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(13);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase14()) {
			// Appuies sur le bouton "case n�14".
			// On v�rifie l'�tat du bouton.
			if (isClicked[13] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase14().setBackground(Color.RED);
				isClicked[13] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(14, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase14().setBackground(null);
				isClicked[13] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(14);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase15()) {
			// Appuies sur le bouton "case n�15".
			// On v�rifie l'�tat du bouton.
			if (isClicked[14] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase15().setBackground(Color.RED);
				isClicked[14] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(15, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase15().setBackground(null);
				isClicked[14] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(15);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase16()) {
			// Appuies sur le bouton "case n�16".
			// On v�rifie l'�tat du bouton.
			if (isClicked[15] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase16().setBackground(Color.RED);
				isClicked[15] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(16, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase16().setBackground(null);
				isClicked[15] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(16);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase17()) {
			// Appuies sur le bouton "case n�17".
			// On v�rifie l'�tat du bouton.
			if (isClicked[16] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase17().setBackground(Color.RED);
				isClicked[16] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(17, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase17().setBackground(null);
				isClicked[16] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(17);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase18()) {
			// Appuies sur le bouton "case n�18".
			// On v�rifie l'�tat du bouton.
			if (isClicked[17] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase18().setBackground(Color.RED);
				isClicked[17] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(18, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase18().setBackground(null);
				isClicked[17] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(18);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase19()) {
			// Appuies sur le bouton "case n�19".
			// On v�rifie l'�tat du bouton.
			if (isClicked[18] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase19().setBackground(Color.RED);
				isClicked[18] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(19, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase19().setBackground(null);
				isClicked[18] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(19);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase20()) {
			// Appuies sur le bouton "case n�20".
			// On v�rifie l'�tat du bouton.
			if (isClicked[19] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase20().setBackground(Color.RED);
				isClicked[19] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(20, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase20().setBackground(null);
				isClicked[19] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(20);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase21()) {
			// Appuies sur le bouton "case n�21".
			// On v�rifie l'�tat du bouton.
			if (isClicked[20] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase21().setBackground(Color.RED);
				isClicked[20] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(21, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase21().setBackground(null);
				isClicked[20] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(21);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase22()) {
			// Appuies sur le bouton "case n�22".
			// On v�rifie l'�tat du bouton.
			if (isClicked[21] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase22().setBackground(Color.RED);
				isClicked[21] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(22, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase22().setBackground(null);
				isClicked[21] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(22);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase23()) {
			// Appuies sur le bouton "case n�23".
			// On v�rifie l'�tat du bouton.
			if (isClicked[22] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase23().setBackground(Color.RED);
				isClicked[22] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(23, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase23().setBackground(null);
				isClicked[22] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(23);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase24()) {
			// Appuies sur le bouton "case n�24".
			// On v�rifie l'�tat du bouton.
			if (isClicked[23] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase24().setBackground(Color.RED);
				isClicked[23] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(24, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase24().setBackground(null);
				isClicked[23] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(24);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase25()) {
			// Appuies sur le bouton "case n�25".
			// On v�rifie l'�tat du bouton.
			if (isClicked[24] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase25().setBackground(Color.RED);
				isClicked[24] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(25, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase25().setBackground(null);
				isClicked[24] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(25);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase26()) {
			// Appuies sur le bouton "case n�26".
			// On v�rifie l'�tat du bouton.
			if (isClicked[25] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase26().setBackground(Color.RED);
				isClicked[25] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(26, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase26().setBackground(null);
				isClicked[25] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(26);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase27()) {
			// Appuies sur le bouton "case n�27".
			// On v�rifie l'�tat du bouton.
			if (isClicked[26] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase27().setBackground(Color.RED);
				isClicked[26] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(27, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase27().setBackground(null);
				isClicked[26] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(27);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase28()) {
			// Appuies sur le bouton "case n�28".
			// On v�rifie l'�tat du bouton.
			if (isClicked[27] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase28().setBackground(Color.RED);
				isClicked[27] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(28, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase28().setBackground(null);
				isClicked[27] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(28);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase29()) {
			// Appuies sur le bouton "case n�29".
			// On v�rifie l'�tat du bouton.
			if (isClicked[28] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase29().setBackground(Color.RED);
				isClicked[28] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(29, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase29().setBackground(null);
				isClicked[28] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(29);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase30()) {
			// Appuies sur le bouton "case n�30".
			// On v�rifie l'�tat du bouton.
			if (isClicked[29] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase30().setBackground(Color.RED);
				isClicked[29] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(30, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase30().setBackground(null);
				isClicked[29] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(30);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase31()) {
			// Appuies sur le bouton "case n�31".
			// On v�rifie l'�tat du bouton.
			if (isClicked[30] == false) {
				this.v.getComponentGame().getTableGame().getCase31().setBackground(Color.RED);
				isClicked[30] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(31, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				this.v.getComponentGame().getTableGame().getCase31().setBackground(null);
				isClicked[30] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(31);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase32()) {
			// Appuies sur le bouton "case n�32".
			// On v�rifie l'�tat du bouton.
			if (isClicked[31] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase32().setBackground(Color.RED);
				isClicked[31] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(32, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase32().setBackground(null);
				isClicked[31] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(32);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase33()) {
			// Appuies sur le bouton "case n�33".
			// On v�rifie l'�tat du bouton.
			if (isClicked[32] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase33().setBackground(Color.RED);
				isClicked[32] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(33, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase33().setBackground(null);
				isClicked[32] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(33);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase34()) {
			// Appuies sur le bouton "case n�34".
			// On v�rifie l'�tat du bouton.
			if (isClicked[33] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase34().setBackground(Color.RED);
				isClicked[33] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(34, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase34().setBackground(null);
				isClicked[33] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(34);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase35()) {
			// Appuies sur le bouton "case n�35".
			// On v�rifie l'�tat du bouton.
			if (isClicked[34] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase35().setBackground(Color.RED);
				isClicked[34] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(35, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase35().setBackground(null);
				isClicked[34] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(35);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		} else if (arg0.getSource() == v.getComponentGame().getTableGame().getCase36()) {
			// Appuies sur le bouton "case n�36".
			// On v�rifie l'�tat du bouton.
			if (isClicked[35] == false) {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase36().setBackground(Color.RED);
				isClicked[35] = true;
				// Ajout du nombre dans le pari.
				// Fen�tre pour le choix de l'argent � parier.
				JOptionPane jop = new JOptionPane();
				if (c.getPlayer().getArgent() >= 0) {
					String val = (String) jop.showInputDialog(null, "Veuillez indiquer votre somme !",
							"Gendarmerie nationale !", JOptionPane.QUESTION_MESSAGE, null, pari, pari[0]);
					if (Integer.parseInt(val) <= c.getPlayer().getArgent()) {
						this.c.getPlayer().getPari().addPari(new PariElement(36, Integer.parseInt(val)));
					}
				}
				System.out.println(this.c.getPlayer().getPari().toString());
			} else {
				// Le bouton change de couleur.
				this.v.getComponentGame().getTableGame().getCase36().setBackground(null);
				isClicked[35] = false;
				// Suppression du nombre dans le Pari.
				this.c.getPlayer().getPari().deletePari(36);
				System.out.println(this.c.getPlayer().getPari().toString());
			}
		}

	}
}
