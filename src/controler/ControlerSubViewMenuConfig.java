package controler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;

import model.client.Client;
import view.SubViewBille;
import view.SubViewHistory;
import view.SubViewPlayer;
import view.SubViewRoulette;
import view.View;
import view.audio.Sound;

/*
 *  Controleur repr�sentant la logique de la vue Accueil.
 */
public class ControlerSubViewMenuConfig implements ActionListener {
	// Composant du controleur.
	View v;
	private Client client;
	private SubViewPlayersThread sub;
	private SubViewRouletteThread subRoulette;
	private SubViewBilleThread subBille;
	private HistoryThread subHist;
	private Timer timerBille;
	private Timer timerRoulette;
	private Timer timer;
	private Timer timerhistorique;

	// Constructeur du controleur.
	public ControlerSubViewMenuConfig(View p_v, Client p_client) {
		this.v = p_v;
		this.client = p_client;
		this.sub = new SubViewPlayersThread(v.getComponentGame().getSubViewPlayer());
		this.subRoulette = new SubViewRouletteThread(v.getComponentGame().getConteneurRouletteBille().getRoulette());
		this.subBille = new SubViewBilleThread(v.getComponentGame().getConteneurRouletteBille().getBille());
		this.subHist = new HistoryThread(v.getComponentGame().getHistory());
	}

	// M�thode d'�coute sur le contr�le : actions effectu�es en appuyant sur les
	// JButtons.
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == v.getComponentAccueil().getSubViewMenuConfig().getConnexion()) {
			// Appuies sur le bouton "mise � jours des serveurs".
			// Bouton d'ajout si salon est disponible.
			JButton btn;
			// Parcours de la liste des boutons � afficher.
			for (int i = 0; i < 5; i++) {
				// On regarde si salon est disponible (poss�de des joueurs).
				try {
					if (client.getRooms().get(i).getNbPlayers() > 0) {
						// On initialise le bouton.
						btn = new JButton(client.getRooms().get(i).getNom());
						// Avec l'indice i, on s�l�ctionne le salon � afficher.
						if (i == 0 && client.getDisponible(0)) {
							// Initialisation de la vue du Salon n�1.
							v.getComponentAccueil().getSubViewGame().setSalon1(btn);
							v.getComponentAccueil().getSubViewGame().getSalon1().addActionListener(this);
							v.getComponentAccueil().getSubViewGame().add(btn);
							client.setDisponible(0, false);
						} else if (i == 1 && client.getDisponible(1)) {
							// Initialisation de la vue du Salon n�2.
							v.getComponentAccueil().getSubViewGame().setSalon2(btn);
							v.getComponentAccueil().getSubViewGame().getSalon2().addActionListener(this);
							v.getComponentAccueil().getSubViewGame().add(btn);
							client.setDisponible(1, false);
						} else if (i == 2 && client.getDisponible(2)) {
							// Initialisation de la vue du Salon n�3.
							v.getComponentAccueil().getSubViewGame().setSalon3(btn);
							v.getComponentAccueil().getSubViewGame().getSalon3().addActionListener(this);
							v.getComponentAccueil().getSubViewGame().add(btn);
							client.setDisponible(2, false);
						} else if (i == 3 && client.getDisponible(3)) {
							// Initialisation de la vue du Salon n�4.
							v.getComponentAccueil().getSubViewGame().setSalon4(btn);
							v.getComponentAccueil().getSubViewGame().getSalon4().addActionListener(this);
							v.getComponentAccueil().getSubViewGame().add(btn);
							client.setDisponible(3, false);
						} else if (i == 4 && client.getDisponible(4)) {
							// Initialisation de la vue du Salon n�5.
							v.getComponentAccueil().getSubViewGame().setSalon5(btn);
							v.getComponentAccueil().getSubViewGame().getSalon5().addActionListener(this);
							v.getComponentAccueil().getSubViewGame().add(btn);
							client.setDisponible(4, false);
						}
					} else {
						// On enl�ve les salons qui n'ont plus de joueurs.
						if (i == 0 && !client.getDisponible(0)) {
							// Suppression du salon n�1.
							// System.out.println("OK");
							v.getComponentAccueil().getSubViewGame()
									.remove(v.getComponentAccueil().getSubViewGame().getSalon1());
							client.setDisponible(0, true);
						} else if (i == 1 && !client.getDisponible(1)) {
							// Suppression du salon n�2.
							v.getComponentAccueil().getSubViewGame()
									.remove(v.getComponentAccueil().getSubViewGame().getSalon2());
							client.setDisponible(1, true);
						} else if (i == 2 && !client.getDisponible(2)) {
							// Suppression du salon n�3.
							v.getComponentAccueil().getSubViewGame()
									.remove(v.getComponentAccueil().getSubViewGame().getSalon3());
							client.setDisponible(2, true);
							;
						} else if (i == 3 && !client.getDisponible(3)) {
							// Suppression du salon n�4.
							v.getComponentAccueil().getSubViewGame()
									.remove(v.getComponentAccueil().getSubViewGame().getSalon4());
							client.setDisponible(3, true);
							;
						} else if (i == 4 && !client.getDisponible(4)) {
							// Suppression du salon n�5.
							v.getComponentAccueil().getSubViewGame()
									.remove(v.getComponentAccueil().getSubViewGame().getSalon5());
							client.setDisponible(4, true);
						}
					}
				} catch (RemoteException e) {
					System.out.println(e.getMessage());
					e.printStackTrace();
				}
			}
			// On revalide et affiche la vue.
			v.revalidate();
			v.repaint();
		} else if (arg0.getSource() == v.getComponentAccueil().getSubViewMenuConfig().getExit()) {
			// Appuies sur le bouton "Quitter".

			// On stop la musique � la fermeture de l'application.
			try {
				Sound.sounds[0].stopa();
				;
			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
			// Fermeture de l'application.
			v.dispose();
		} else if (arg0.getSource() == v.getComponentAccueil().getSubViewMenuConfig().getCreate()) {
			// Appuie sur le bouton "Cr�ation d'un salon".
			// Num�ro du salon disponible.

			int numsalon = client.createSalon();
			// System.out.println("Salon numero" + numsalon + " a cr��");

			// On regarde que le salon renvoy� est disponible pour la cr�ation.
			if (numsalon != -1) {
				// On initialise le bouton.
				JButton btn = new JButton();
				try {
					btn.setText(client.getPlayer().getRoom().getNom());
				} catch (RemoteException e) {
					System.out.println(e.getMessage());
					e.printStackTrace();
				}
				// On initialise la vue du salon disponible.
				if (numsalon == 0) {
					// Salon n�1.
					v.getComponentAccueil().getSubViewGame().setSalon1(btn);
					v.getComponentAccueil().getSubViewGame().getSalon1().addActionListener(this);
					client.setDisponible(0, false);
				} else if (numsalon == 1) {
					// Salon n�2.
					v.getComponentAccueil().getSubViewGame().setSalon2(btn);
					v.getComponentAccueil().getSubViewGame().getSalon2().addActionListener(this);
					client.setDisponible(1, false);
				} else if (numsalon == 2) {
					// Salon n�3.
					v.getComponentAccueil().getSubViewGame().setSalon3(btn);
					v.getComponentAccueil().getSubViewGame().getSalon3().addActionListener(this);
					client.setDisponible(2, false);
				} else if (numsalon == 3) {
					// Salon n�4.
					v.getComponentAccueil().getSubViewGame().setSalon4(btn);
					v.getComponentAccueil().getSubViewGame().getSalon4().addActionListener(this);
					client.setDisponible(3, false);
				} else if (numsalon == 4) {
					// Salon n�5.
					v.getComponentAccueil().getSubViewGame().setSalon5(btn);
					v.getComponentAccueil().getSubViewGame().getSalon5().addActionListener(this);
					client.setDisponible(4, false);
				}
				// On affiche le salon.
				v.getComponentAccueil().getSubViewGame().add(btn);
				// On initialise la vue pour le retour du salon et les autres clients.
				try {
					// Parcours de la liste des boutons � afficher.
					for (int i = 0; i < 5; i++) {
						// On regarde si salon est disponible (poss�de des joueurs).
						if (client.getRooms().get(i).getNbPlayers() > 0) {
							// On initialise le bouton.
							btn = new JButton(client.getRooms().get(i).getNom());
							// Avec l'indice i, on s�l�ctionne le salon � afficher.
							if (i == 0 && client.getDisponible(0)) {
								// Initialisation de la vue du Salon n�1.
								// System.out.println("ARCHITECTURE" + i);
								v.getComponentAccueil().getSubViewGame().setSalon1(btn);
								v.getComponentAccueil().getSubViewGame().getSalon1().addActionListener(this);
								v.getComponentAccueil().getSubViewGame().add(btn);
								client.setDisponible(0, false);
								client.setActualise(true);
							} else if (i == 1 && client.getDisponible(1)) {
								// Initialisation de la vue du Salon n�2.
								v.getComponentAccueil().getSubViewGame().setSalon2(btn);
								v.getComponentAccueil().getSubViewGame().getSalon2().addActionListener(this);
								v.getComponentAccueil().getSubViewGame().add(btn);
								client.setDisponible(1, false);
							} else if (i == 2 && client.getDisponible(2)) {
								// Initialisation de la vue du Salon n�3.
								// System.out.println("ARCHITECTURE" + i);
								v.getComponentAccueil().getSubViewGame().setSalon3(btn);
								v.getComponentAccueil().getSubViewGame().getSalon3().addActionListener(this);
								v.getComponentAccueil().getSubViewGame().add(btn);
								client.setDisponible(2, false);
							} else if (i == 3 && client.getDisponible(3)) {
								// Initialisation de la vue du Salon n�4.
								// System.out.println("ARCHITECTURE" + i);
								v.getComponentAccueil().getSubViewGame().setSalon4(btn);
								v.getComponentAccueil().getSubViewGame().getSalon4().addActionListener(this);
								v.getComponentAccueil().getSubViewGame().add(btn);
								client.setDisponible(3, false);
							} else if (i == 4 && client.getDisponible(4)) {
								// Initialisation de la vue du Salon n�5.
								// System.out.println("ARCHITECTURE" + i);
								v.getComponentAccueil().getSubViewGame().setSalon5(btn);
								v.getComponentAccueil().getSubViewGame().getSalon5().addActionListener(this);
								v.getComponentAccueil().getSubViewGame().add(btn);
								client.setDisponible(4, false);
							}
						}
					}
				} catch (RemoteException e) {
					System.out.println(e.getMessage());
					e.printStackTrace();
				}
				// On initialise la vue des profils des joueurs (nom et argent disponible).
				v.getComponentGame().getSubViewPlayer().setNameJoueur1(
						"   " + this.client.getPlayer().getName() + "( " + this.client.getPlayer().getArgent() + " )");
				v.getComponentGame().getSubViewProfil().setTodoName(this.client.getPlayer().getName());
				v.getComponentGame().getSubViewProfil().setArgent(this.client.getPlayer().getArgent());
				// On change de contexte + activation de la musique.
				try {
					client.getPlayer().getRoom()
							.addMessage("Le joueur " + client.getPlayer().getName() + " a rejoint le salon !");
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				v.changementContexte(v.getComponentGame());
				v.getComponentGame().revalidate();
				v.repaint();
				this.timer = new Timer();
				this.sub = new SubViewPlayersThread(v.getComponentGame().getSubViewPlayer());
				client.setActualise(true);
				timer.scheduleAtFixedRate(sub, 0, 1000);
				this.timerRoulette = new Timer();
				this.subRoulette = new SubViewRouletteThread(
						v.getComponentGame().getConteneurRouletteBille().getRoulette());
				timerRoulette.scheduleAtFixedRate(subRoulette, 0, 1000);
				this.timerBille = new Timer();
				this.subBille = new SubViewBilleThread(v.getComponentGame().getConteneurRouletteBille().getBille());
				timerBille.scheduleAtFixedRate(subBille, 0, 20);
				this.timerhistorique = new Timer();
				this.subHist = new HistoryThread(v.getComponentGame().getHistory());
				timerhistorique.scheduleAtFixedRate(subHist, 0, 1000);
				Sound.sounds[0].run();
			} else {
				System.out.println("Impossible d'avoir plus de 5 salons.");
			}
		} else if (arg0.getSource() == v.getComponentAccueil().getSubViewGame().getSalon1()) {
			// Op�ration effectu�e lorsque le bouton du salon n�1 est appuy�.

			// Initialisation des param�tres pour la vue "Profil"(nom et argent).
			v.getComponentGame().getSubViewProfil().setTodoName(this.client.getPlayer().getName());
			v.getComponentGame().getSubViewProfil().setArgent(this.client.getPlayer().getArgent());

			// Ajout du joueur par le client dans le salon.
			try {
				this.client.addPLayer(0);
				// System.out.println("player addedd : " +
				// this.client.getRooms().get(0).toStringAllJoueurs() + "number "
				// + this.client.getRooms().get(0).getNbPlayers());
			} catch (RemoteException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
			// Changement de contexte + activation musique.
			try {
				client.getPlayer().getRoom()
						.addMessage("Le joueur " + client.getPlayer().getName() + " a rejoint le salon !");
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			v.changementContexte(v.getComponentGame());
			v.repaint();
			this.timer = new Timer();
			this.sub = new SubViewPlayersThread(v.getComponentGame().getSubViewPlayer());
			client.setActualise(true);
			timer.scheduleAtFixedRate(sub, 0, 1000);
			this.timerRoulette = new Timer();
			this.subRoulette = new SubViewRouletteThread(
					v.getComponentGame().getConteneurRouletteBille().getRoulette());
			timerRoulette.scheduleAtFixedRate(subRoulette, 0, 1000);
			this.timerBille = new Timer();
			this.subBille = new SubViewBilleThread(v.getComponentGame().getConteneurRouletteBille().getBille());
			timerBille.scheduleAtFixedRate(subBille, 0, 20);
			this.timerhistorique = new Timer();
			this.subHist = new HistoryThread(v.getComponentGame().getHistory());
			timerhistorique.scheduleAtFixedRate(subHist, 0, 1000);
			Sound.sounds[0].run();
		} else if (arg0.getSource() == v.getComponentAccueil().getSubViewGame().getSalon2()) {
			// Op�ration effectu�e lorsque le bouton du salon n�2 est appuy�.

			// Initialisation des param�tres pour la vue "Profil"(nom et argent).
			v.getComponentGame().getSubViewProfil().setTodoName(this.client.getPlayer().getName());
			v.getComponentGame().getSubViewProfil().setArgent(this.client.getPlayer().getArgent());

			// Ajout du joueur par le client dans le salon.
			try {
				this.client.addPLayer(1);
				// System.out.println("player added : " +
				// this.client.getRooms().get(1).toStringAllJoueurs());
			} catch (RemoteException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
			// Changement de contexte + activation musique.
			try {
				client.getPlayer().getRoom()
						.addMessage("Le joueur " + client.getPlayer().getName() + " a rejoint le salon !");
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			v.changementContexte(v.getComponentGame());
			v.repaint();
			this.timer = new Timer();
			this.sub = new SubViewPlayersThread(v.getComponentGame().getSubViewPlayer());
			client.setActualise(true);
			timer.scheduleAtFixedRate(sub, 0, 1000);
			this.timerRoulette = new Timer();
			this.subRoulette = new SubViewRouletteThread(
					v.getComponentGame().getConteneurRouletteBille().getRoulette());
			timerRoulette.scheduleAtFixedRate(subRoulette, 0, 1000);
			this.timerBille = new Timer();
			this.subBille = new SubViewBilleThread(v.getComponentGame().getConteneurRouletteBille().getBille());
			timerBille.scheduleAtFixedRate(subBille, 0, 20);
			this.timerhistorique = new Timer();
			this.subHist = new HistoryThread(v.getComponentGame().getHistory());
			timerhistorique.scheduleAtFixedRate(subHist, 0, 1000);
			Sound.sounds[0].run();
		} else if (arg0.getSource() == v.getComponentAccueil().getSubViewGame().getSalon3()) {
			// Op�ration effectu�e lorsque le bouton du salon n�3 est appuy�.

			// Initialisation des param�tres pour la vue "Profil"(nom et argent).
			v.getComponentGame().getSubViewProfil().setTodoName(this.client.getPlayer().getName());
			v.getComponentGame().getSubViewProfil().setArgent(this.client.getPlayer().getArgent());

			// Ajout du joueur par le client dans le salon.
			try {
				this.client.addPLayer(2);
				// System.out.println("player added : " +
				// this.client.getRooms().get(2).toStringAllJoueurs());
			} catch (RemoteException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
			// Changement de contexte + activation musique.
			try {
				client.getPlayer().getRoom()
						.addMessage("Le joueur " + client.getPlayer().getName() + " a rejoint le salon !");
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			v.changementContexte(v.getComponentGame());
			v.repaint();
			this.timer = new Timer();
			this.sub = new SubViewPlayersThread(v.getComponentGame().getSubViewPlayer());
			client.setActualise(true);
			timer.scheduleAtFixedRate(sub, 0, 1000);
			this.timerRoulette = new Timer();
			this.subRoulette = new SubViewRouletteThread(
					v.getComponentGame().getConteneurRouletteBille().getRoulette());
			timerRoulette.scheduleAtFixedRate(subRoulette, 0, 1000);
			this.timerBille = new Timer();
			this.subBille = new SubViewBilleThread(v.getComponentGame().getConteneurRouletteBille().getBille());
			timerBille.scheduleAtFixedRate(subBille, 0, 20);
			this.timerhistorique = new Timer();
			this.subHist = new HistoryThread(v.getComponentGame().getHistory());
			timerhistorique.scheduleAtFixedRate(subHist, 0, 1000);
			Sound.sounds[0].run();
		} else if (arg0.getSource() == v.getComponentAccueil().getSubViewGame().getSalon4()) {
			// Op�ration effectu�e lorsque le bouton du salon n�4 est appuy�.

			// Initialisation des param�tres pour la vue "Profil"(nom et argent).
			v.getComponentGame().getSubViewProfil().setTodoName(this.client.getPlayer().getName());
			v.getComponentGame().getSubViewProfil().setArgent(this.client.getPlayer().getArgent());

			// Ajout du joueur par le client dans le salon.
			try {
				this.client.addPLayer(3);
				// System.out.println("player added : " +
				// this.client.getRooms().get(3).toStringAllJoueurs());
			} catch (RemoteException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
			// Changement de contexte + activation musique.
			try {
				client.getPlayer().getRoom()
						.addMessage("Le joueur " + client.getPlayer().getName() + " a rejoint le salon !");
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			v.changementContexte(v.getComponentGame());
			v.repaint();
			this.timer = new Timer();
			this.sub = new SubViewPlayersThread(v.getComponentGame().getSubViewPlayer());
			client.setActualise(true);
			timer.scheduleAtFixedRate(sub, 0, 1000);
			this.timerRoulette = new Timer();
			this.subRoulette = new SubViewRouletteThread(
					v.getComponentGame().getConteneurRouletteBille().getRoulette());
			timerRoulette.scheduleAtFixedRate(subRoulette, 0, 1000);
			this.timerBille = new Timer();
			this.subBille = new SubViewBilleThread(v.getComponentGame().getConteneurRouletteBille().getBille());
			timerBille.scheduleAtFixedRate(subBille, 0, 20);
			this.timerhistorique = new Timer();
			this.subHist = new HistoryThread(v.getComponentGame().getHistory());
			timerhistorique.scheduleAtFixedRate(subHist, 0, 1000);
			Sound.sounds[0].run();
		} else if (arg0.getSource() == v.getComponentAccueil().getSubViewGame().getSalon5()) {
			// Op�ration effectu�e lorsque le bouton du salon n�5 est appuy�.

			// Initialisation des param�tres pour la vue "Profil"(nom et argent).
			this.client.getPlayer().setName(v.getComponentConnexion().getInName().getText());
			this.client.getPlayer().setArgent(
					Integer.parseInt((String) v.getComponentConnexion().getChooseMise().getSelectedItem().toString()));
			v.getComponentGame().getSubViewProfil().setTodoName(this.client.getPlayer().getName());
			v.getComponentGame().getSubViewProfil().setArgent(this.client.getPlayer().getArgent());

			// Ajout du joueur par le client dans le salon.
			try {
				this.client.addPLayer(4);
				// System.out.println("player added : " +
				// this.client.getRooms().get(4).toStringAllJoueurs());
			} catch (RemoteException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
			// Changement de contexte + activation musique.
			try {
				client.getPlayer().getRoom()
						.addMessage("Le joueur " + client.getPlayer().getName() + " a rejoint le salon !");
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			v.changementContexte(v.getComponentGame());
			v.repaint();
			this.timer = new Timer();
			this.sub = new SubViewPlayersThread(v.getComponentGame().getSubViewPlayer());
			client.setActualise(true);
			timer.scheduleAtFixedRate(sub, 0, 1000);
			this.timerRoulette = new Timer();
			this.subRoulette = new SubViewRouletteThread(
					v.getComponentGame().getConteneurRouletteBille().getRoulette());
			timerRoulette.scheduleAtFixedRate(subRoulette, 0, 1000);
			this.timerBille = new Timer();
			this.subBille = new SubViewBilleThread(v.getComponentGame().getConteneurRouletteBille().getBille());
			timerBille.scheduleAtFixedRate(subBille, 0, 20);
			this.timerhistorique = new Timer();
			this.subHist = new HistoryThread(v.getComponentGame().getHistory());
			timerhistorique.scheduleAtFixedRate(subHist, 0, 1000);
			Sound.sounds[0].run();
		}
	}

	public class SubViewPlayersThread extends TimerTask {
		private SubViewPlayer players;

		public SubViewPlayersThread(SubViewPlayer pPlayers) {
			this.players = pPlayers;
			new ArrayList<String>();
		}

		public void setPlayerlist(ArrayList<String> playerlist) {
		}

		public void run() {
			try {
				if (client.isActualise()) {
					players.raffraichissement(client.getPlayer().getRoom().getPlayers(),
							client.getPlayer().getRoom().getNbPlayers());
					// System.out.println("en cours");
				} else {
					timer.cancel();
					// System.out.println("je stop");
				}
				// System.out.println("je tourne");
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public class SubViewRouletteThread extends TimerTask {
		// Composant de la classe "ComponentGameThread".
		private SubViewRoulette horloge;

		// Constructeur de la classe "ComponentGameThread".
		SubViewRouletteThread(SubViewRoulette horloge) {
			this.horloge = horloge;
		}

		// M�thode main du thread effectuant la rotation en d�calant les nombre du
		// tableau.
		public void run() {
			try {
				client.getPlayer().setMaster();
				if (client.isActualise()) {
					if (client.getPlayer().getMaster()) {
						if (client.getPlayer().getRoom().getBille() != null) {
							if (!client.getPlayer().getRoom().getBille().getStopped()) {
								client.getPlayer().getRoom().getRoulette().rafraichissementRoulette();
								horloge.rafraichissement(client.getPlayer().getRoom().getRoulette().getEncoches());
								// System.out.println("num�ro pr�sent dans la roulette : D�but");
								// for (int i = 0; i < 36; i++) {
								// System.out.print(client.getPlayer().getRoom().getRoulette().getEncoches().get(i).getNumber()
								// + " ");
								// }
								// System.out.println("FIN");
							}
						}
					} else {
						horloge.rafraichissement(client.getPlayer().getRoom().getRoulette().getEncoches());
					}
				} else {
					timerRoulette.cancel();
					System.out.println("FIN roulette");
				}
				if (client.getPlayer().getRoom() != null) {
					/*
					 * 
					 * System.out.println("master ? : " + client.getPlayer().getMaster() +
					 * 
					 * "name master " + client.getPlayer().getRoom().getMaster().getName() +
					 * "master name" + client.getPlayer().getRoom().getMasterName());
					 */
				}

			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}

	public class SubViewBilleThread extends TimerTask {
		// Composant de la classe "ComponentGameThread".
		private SubViewBille bille;
		int nextX;
		int nextY;
		long t = System.currentTimeMillis();

		// Constructeur de la classe "ComponentGameThread".
		SubViewBilleThread(SubViewBille bille) {
			this.bille = bille;
		}

		// M�thode main du thread effectuant la rotation en d�calant les nombre du
		// tableau.
		public void run() {
			try {
				// Attributs.
				client.getPlayer().setMaster();
				double cX = (v.getComponentGame().getConteneurRouletteBille().getRoulette().getWidth() / 2 - 35);
				double cY = (v.getComponentGame().getConteneurRouletteBille().getRoulette().getHeight() / 2 - 35);
				double nextX = 0;
				double nextY = 0;
				double x = 0;
				double y = 0;
				double angle = Math.toRadians(1);
				int delay = 1000;

				// Test si le client est dans un salon.
				if (client.isActualise()) {

					// Test si le client est le master de la room.
					if (client.getPlayer().getMaster()) {
						// Test si la bille existe.
						if (client.getPlayer().getRoom().getBille() != null) {
							// Recalcul de la position de la bille.
							if (!client.getPlayer().getRoom().getBille().getStopped()) {
								x = client.getPlayer().getRoom().getBille().getX();
								y = client.getPlayer().getRoom().getBille().getY();
								// Application de la fonction de rotation.
								nextX = (cX + (x - cX) * Math.cos(angle) - (y - cY) * Math.sin(angle));
								nextY = (cY + (x - cX) * Math.sin(angle) + (y - cY) * Math.cos(angle));
								// Actualise la bille cot� client.
								bille.rafraichissement((int) nextX, (int) nextY);
								// Actualise la bille cot� serveur.
								if (!client.getPlayer().getRoom().getBille().getStopped()) {
									client.getPlayer().getRoom().getBille().setX(nextX);
									client.getPlayer().getRoom().getBille().setY(nextY);
								}
								// Arrete la bille tous les "delay".
								if (System.currentTimeMillis() - t >= delay) {
									nearestEncoche();
									client.getPlayer().getRoom().getBille().setStopped(true);

									client.getPlayer().getRoom().addMessage("La bille se trouve sur l'encoche : "
											+ client.getPlayer().getRoom().getBille().getEncoche().getNumber());
									Thread.sleep(1000);
									this.t = System.currentTimeMillis();
									// Redemarre la bille.
									if (client.isActualise())
										client.getPlayer().getRoom().getBille().setStopped(false);
								}
							}
						}
						// Si le client n'es pas le master.
					} else {
						// Actualisation de la bille cot� client.
						if (client.getPlayer().getRoom().getBille() != null) {
							nextX = client.getPlayer().getRoom().getBille().getX();
							nextY = client.getPlayer().getRoom().getBille().getY();
							bille.rafraichissement((int) nextX, (int) nextY);
						}
					}
					// Arret de la bille (fin salon)
				} else {
					System.out.println("FIN Bille");
					timerBille.cancel();
				}
			} catch (

			Exception e) {
				e.printStackTrace();
			}
		}

		// Retourne l'encoche sur laquelle la bille se trouve apr�s une rotation.
		public void nearestEncoche() throws RemoteException {
			// Attributs.
			int[] tabX = v.getComponentGame().getConteneurRouletteBille().getRoulette().getTableauEntierPosX();
			int[] tabY = v.getComponentGame().getConteneurRouletteBille().getRoulette().getTableauEntierPosY();
			int save = 0;
			double x = client.getPlayer().getRoom().getBille().getX();
			double y = client.getPlayer().getRoom().getBille().getY();
			double distance = Math.sqrt(Math.pow(x - tabX[0], 2) + Math.pow(y - tabY[0], 2));
			// Calcule l'encoche la plus proche.
			for (int i = 1; i < v.getComponentGame().getConteneurRouletteBille().getRoulette()
					.getTableauEntierPosX().length; i++) {
				if (distance >= Math.sqrt(Math.pow(x - tabX[i], 2) + Math.pow(y - tabY[i], 2))) {
					distance = Math.sqrt(Math.pow(x - tabX[i], 2) + Math.pow(y - tabY[i], 2));
					save = i;
				}
			}

			// Sauvegarde l'encoche dans la bille.
			client.getPlayer().getRoom().getBille()
					.setEncoche(client.getPlayer().getRoom().getRoulette().getEncoches().get(save));
			;

		}
	}

	/*
	 * Classe de thread pour l'historique.
	 */
	public class HistoryThread extends TimerTask {
		// Vue de l'historique.
		private SubViewHistory history;

		// Constructeur de la classe.
		public HistoryThread(SubViewHistory pHistory) {
			this.history = pHistory;
			new ArrayList<String>();
		}

		// Thread.
		public void run() {
			try {
				// Test si le client est dans un salon.
				if (client.isActualise()) {
					// Actualise l'historique des joueurs.
					history.raffraichissement(client.getPlayer().getRoom().getHistorique());
				} else {
					// On stop le thread.
					timerBille.cancel();
				}
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
