package controler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.client.Client;
import view.View;

/*
 *  Controleur repr�sentant la logique de la vue Connexion (vue d'initialisation de l'application).
 */
public class ControlerComponentConnexion implements ActionListener {
	// Composant du controleur.
	View v;
	Client c;

	// Constructeur du controleur.
	public ControlerComponentConnexion(View pV, Client pC) {
		this.v = pV;
		this.c = pC;
	}

	// M�thode d'�coute sur le contr�le : actions effectu�es en appuyant sur les
	// JButtons.
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == v.getComponentConnexion().getValid()) {
			// Appuies sur le bouton "validation".
			if (!(v.getComponentConnexion().getInName().getText().length() == 0)) {
				// Pesudo non-vide.
				// Initialisation du pseudo et l'argent.
				this.c.getPlayer().setName(v.getComponentConnexion().getInName().getText());
				this.c.getPlayer().setArgent(Integer
						.parseInt((String) v.getComponentConnexion().getChooseMise().getSelectedItem().toString()));
				// Initialisation de la vue.
				v.getComponentAccueil().getSubViewProfil().setTodoName(this.c.getPlayer().getName());
				v.getComponentAccueil().getSubViewProfil().setArgent(this.c.getPlayer().getArgent());
				// Changement de contexte : Connexion -> Accueil.
				v.changementContexte(v.getComponentAccueil());
				v.repaint();
			}
		}
	}
}
