package controler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

import model.client.Client;
import view.View;
import view.audio.Sound;

/*
 * Controleur repr�sentant la logique de la vue du Jeux.
 */
public class ControlerComponentGame implements ActionListener {
	// Composant du controleur.
	View v;
	Client client;
	PariThread t = new PariThread();
	boolean tstarted;

	// Constructeur du controleur.
	public ControlerComponentGame(View pV, Client pC) {
		this.v = pV;
		this.client = pC;

	}

	// M�thode d'�coute sur le contr�le : actions effectu�es en appuyant sur les
	// JButtons.
	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == v.getComponentGame().getExitGame()) {
			this.client.setActualise(false);
			// Appuies sur le bouton "Quitter le salon".
			// On parcours les 5 salons disponibles.
			for (int i = 0; i < 5; i++) {
				// On enl�ve les salons qui n'ont plus de joueurs.
				if (i == 0 && !this.client.getDisponible(0)) {
					// Suppression du salon n�1.
					v.getComponentAccueil().getSubViewGame()
							.remove(v.getComponentAccueil().getSubViewGame().getSalon1());
					this.client.setDisponible(0, true);
				} else if (i == 1 && !this.client.getDisponible(1)) {
					// Suppression du salon n�2.
					v.getComponentAccueil().getSubViewGame()
							.remove(v.getComponentAccueil().getSubViewGame().getSalon2());
					this.client.setDisponible(1, true);
				} else if (i == 2 && !this.client.getDisponible(2)) {
					// Suppression du salon n�3.
					v.getComponentAccueil().getSubViewGame()
							.remove(v.getComponentAccueil().getSubViewGame().getSalon3());
					this.client.setDisponible(2, true);
				} else if (i == 3 && !this.client.getDisponible(3)) {
					// Suppression du salon n�4.
					v.getComponentAccueil().getSubViewGame()
							.remove(v.getComponentAccueil().getSubViewGame().getSalon4());
					this.client.setDisponible(3, true);
				} else if (i == 4 && !this.client.getDisponible(4)) {
					// Suppression du salon n�5.
					v.getComponentAccueil().getSubViewGame()
							.remove(v.getComponentAccueil().getSubViewGame().getSalon5());
					this.client.setDisponible(4, true);
				}
			}

			try {
				client.getPlayer().getRoom()
						.addMessage("Le joueur " + client.getPlayer().getName() + " a quitt� le salon !");
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// �coute sur le bouton "Exit" -> ComponantAccueil !
			Sound.sounds[0].stopa();
			client.deleteRoom();
			v.changementContexte(v.getComponentAccueil());
			v.repaint();
		} else if (arg0.getSource() == v.getComponentGame().getValidPari()) {
			// Appuies sur le bouton "Pari".
			// Test si le joueur poss�de assez d'argent pour parier.
			int somme = 0;
			for (int i = 0; i < client.getPlayer().getPari().getPari().size(); i++) {
				somme += client.getPlayer().getPari().getPari().get(i).getSomme();
			}
			if (client.getPlayer().getArgent() - somme >= 0) {
				// Test si un pari d'un joueur est d�j� en cours pour le tour de jeu.
				if (!tstarted) {
					tstarted = true;
					t = new PariThread();
					t.start();
				} else {
					try {
						client.getPlayer().getRoom()
								.addMessage("Le joueur " + client.getPlayer().getName() + " a d�j� un Pari en cours !");
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				try {
					client.getPlayer().getRoom()
							.addMessage("Le joueur " + client.getPlayer().getName() + " est pauvre !");
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	/*
	 * Classe de thread pour le pari.
	 */
	public class PariThread extends Thread {
		// Constructeur de la classe "ComponentGameThread".
		public PariThread() {
		}

		public synchronized void run() {
			try {
				// Retire l'argent du joueur pour chaque pari effectu�.
				for (int i = 0; i < client.getPlayer().getPari().getPari().size(); i++) {
					client.getPlayer().setArgent(
							client.getPlayer().getArgent() - client.getPlayer().getPari().getPari().get(i).getSomme());
				}
				v.getComponentGame().getSubViewProfil().setArgent(client.getPlayer().getArgent());
				v.repaint();
				client.getPlayer().getRoom()
						.addMessage("Le joueur " + client.getPlayer().getName() + " vient de parier !");
				// Attend que la bille s'arr�te.
				while (!client.getPlayer().getRoom().getBille().getStopped()) {
				}
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// Donne l'argent au joueur si il a gagn�.
			for (int i = 0; i < client.getPlayer().getPari().getPari().size(); i++) {
				try {
					if (client.getPlayer().getPari().getPari().get(i).getNumero() == client.getPlayer().getRoom()
							.getBille().getEncoche().getNumber()) {
						client.getPlayer().setArgent(client.getPlayer().getArgent()
								+ client.getPlayer().getPari().getPari().get(i).getSomme() * 2);
						client.getPlayer().getRoom().addMessage(client.getPlayer().getName() + " vient de gagner "
								+ (client.getPlayer().getPari().getPari().get(i).getSomme() * 2) + " !");
					}
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			tstarted = false;
			v.getComponentGame().getSubViewProfil().setArgent(client.getPlayer().getArgent());
			// On actualise la vue apr�s avoir effectu� les op�rations.
			v.repaint();
		}
	}

}
