package model.client;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;

import controler.ControlerComponentConnexion;
import controler.ControlerComponentGame;
import controler.ControlerComponentSubViewTableGame;
import controler.ControlerSubViewMenuConfig;
import model.entity.ISalon;
import model.entity.Joueur;
import model.game.IGame;
import model.server.Server;
import view.View;

@SuppressWarnings("serial")
public class Client extends Thread implements Serializable {

	IGame game;
	boolean gameStarted;
	boolean connectedToServer;
	Joueur player;
	// Indique les salon a afficher.
	Boolean[] est_disponible = { true, true, true, true, true };
	// Indique si le client est dans un salon
	boolean actualise;

	// Constructeur
	public Client(String pName, int pArgent) throws RemoteException {
		this.game = null;
		this.gameStarted = false;
		this.connectedToServer = false;
		this.player = new Joueur(pName, pArgent);
		this.actualise = false;
	}

	// getteurs et setteurs
	public boolean isActualise() {
		return actualise;
	}

	public void setActualise(boolean actualise) {
		this.actualise = actualise;
	}

	public Boolean getDisponible(int salonnumber) {
		return est_disponible[salonnumber];
	}

	public void setDisponible(int salonnumber, Boolean est_disponible) {
		this.est_disponible[salonnumber] = est_disponible;
	}

	public Joueur getPlayer() {
		return player;
	}

	public ArrayList<ISalon> getRooms() throws RemoteException {
		return game.getRooms();
	}

	public void addPLayer(int roomnumber) throws RemoteException {

		game.getRooms().get(roomnumber).addPlayer(this.player);
		this.player.setRoom(game.getRooms().get(roomnumber));

	}

	// Lorsque le joueur rentre dans un salon.
	public int createSalon() {
		try {
			// Recup�re le num�ro du salon a cr�er.
			int numsalon = game.creerSalon(player, this);
			// D�finit le joueur en tant que master du salon.
			if (numsalon != -1) {
				player.setRoom(game.getRoom(numsalon));
				player.getRoom().setMaster();
				player.setMaster();
			}
			return numsalon;
		} catch (Exception e) {
			return -1;
		}
	}

	// Lorsque le joueur quitte un salon.
	public void deleteRoom() {
		try {
			game.deleteRoom(player.getRoom(), player);
			player.deleteRoom();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {

		try {

			while (!Thread.interrupted()) {

				// Test si on n'est pas connect� au serveur.
				if (!connectedToServer) {
					try {
						game = (IGame) Naming.lookup("rmi://localhost:5000/game");
						System.out.println("Connected to server");
						connectedToServer = true;

					} catch (MalformedURLException | RemoteException | NotBoundException e) {
						// TODO Auto-generated catch block
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Classe principale.
	public static void main(String args[]) {
		try {
			// Attributs.
			// Client.
			Client client = new Client("", 0);
			// Serveur.
			Server serveur = new Server();
			// Vue principale.
			View view = new View();
			// Initialisation de la vue.
			view.init();
			// Lancement du serveur.
			serveur.start();
			// Lancement du client.
			client.start();
			// D�claration des controlleurs.
			ControlerSubViewMenuConfig c1 = new ControlerSubViewMenuConfig(view, client);
			ControlerComponentConnexion c2 = new ControlerComponentConnexion(view, client);
			ControlerComponentGame c3 = new ControlerComponentGame(view, client);
			ControlerComponentSubViewTableGame c4 = new ControlerComponentSubViewTableGame(view, client);

			// Initialisation des �v�nements.
			view.getComponentAccueil().getSubViewMenuConfig().getConnexion().addActionListener(c1);
			view.getComponentAccueil().getSubViewMenuConfig().getExit().addActionListener(c1);
			view.getComponentAccueil().getSubViewMenuConfig().getCreate().addActionListener(c1);
			view.getComponentGame().getExitGame().addActionListener(c3);
			view.getComponentGame().getValidPari().addActionListener(c3);
			view.getComponentConnexion().getValid().addActionListener(c2);

			view.getComponentGame().getTableGame().getCase1().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase2().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase3().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase4().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase5().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase6().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase7().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase8().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase9().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase10().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase11().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase12().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase13().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase14().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase15().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase16().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase17().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase18().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase19().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase20().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase21().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase22().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase23().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase24().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase25().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase26().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase27().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase28().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase29().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase30().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase31().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase32().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase33().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase34().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase35().addActionListener(c4);
			view.getComponentGame().getTableGame().getCase36().addActionListener(c4);

		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
