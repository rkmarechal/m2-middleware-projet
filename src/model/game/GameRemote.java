package model.game;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import model.client.Client;
import model.entity.ISalon;
import model.entity.Joueur;
import model.entity.Salon;

/*
 * Classe GameRemote.
 */
@SuppressWarnings("serial")
public class GameRemote extends UnicastRemoteObject implements IGame {
	// Attributs.
	private ArrayList<ISalon> rooms;
	private ArrayList<Client> clients;
	private int nbPlayers;
	private int NB_PLAYER_LIMIT = 4;
	private boolean fullRoom[] = { false, false, false, false, false };

	// Constructeur de la classe.
	public GameRemote() throws RemoteException {
		super();
		nbPlayers = 0;
		rooms = new ArrayList<ISalon>();
		clients = new ArrayList<Client>();
		initrooms();
	}

	// M�thode pour initialiser les salons.
	public void initrooms() throws RemoteException {
		for (int i = 0; i < 5; i++) {
			String roomname;
			roomname = "Salon n�" + Integer.toString(i);
			Salon room = new Salon(roomname);
			rooms.add(room);
		}
	}

	// Getteur et Setteur
	public boolean[] getBooleanRooms() throws RemoteException {
		return this.getFullRoom();
	}

	public ISalon getRoom(int roomnumber) throws RemoteException {
		return rooms.get(roomnumber);
	}

	public ArrayList<ISalon> getRooms() throws RemoteException {
		return this.rooms;
	}

	public boolean[] getFullRoom() {
		return fullRoom;
	}

	public void setFullRoomOuvert(int i) {
		this.fullRoom[i] = true;
	}

	public boolean getFullRoomI(int i) {
		return this.fullRoom[i];
	}

	// M�thode pour la cr�ation des salons lorsqu'au minimum un joueur rejoin.
	public int creerSalon(Joueur player, Client client) throws RemoteException {
		this.clients.add(client);
		// Parcours les salons.
		for (int i = 0; i < 5; i++) {
			// Test si un salon est disponible.
			if (rooms.get(i).getNbPlayers() == 0) {
				rooms.get(i).addPlayer(player);
				this.fullRoom[i] = true;
				return i;
			}
		}
		return -1;
	}

	@Override
	// M�thode pour la suppression d'un salon.
	public void deleteRoom(ISalon room, Joueur player) throws RemoteException {
		for (int i = 0; i < rooms.size(); i++) {
			if (this.rooms.get(i).getNom().equals(room.getNom())) {
				this.rooms.get(i).deletePlayer(player);
			}
		}
	}

	@Override
	// M�thode testant si le nombre de joueur est atteint.
	public boolean fullNbPlayersReached() {
		return (nbPlayers == NB_PLAYER_LIMIT);
	}
}
