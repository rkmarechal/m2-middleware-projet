package model.game;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

import model.client.Client;
import model.entity.ISalon;
import model.entity.Joueur;

/*
 * Interface de la classe GameRemote.
 */
public interface IGame extends Remote {

	public boolean fullNbPlayersReached() throws RemoteException;

	public int creerSalon(Joueur player, Client client) throws RemoteException;

	public ArrayList<ISalon> getRooms() throws RemoteException;

	public ISalon getRoom(int roomnumber) throws RemoteException;

	public void setFullRoomOuvert(int i) throws RemoteException;

	public boolean getFullRoomI(int i) throws RemoteException;

	public boolean[] getBooleanRooms() throws RemoteException;

	public void deleteRoom(ISalon room, Joueur player) throws RemoteException;
}
