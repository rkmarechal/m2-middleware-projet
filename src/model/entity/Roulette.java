package model.entity;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Random;

/*
 * Classe Roulette.
 */
@SuppressWarnings("serial")
public class Roulette extends UnicastRemoteObject implements IRoulette {
	// Attributs
	private ArrayList<IEncoche> encoches = new ArrayList<IEncoche>();

	// Constructeur.
	public Roulette() throws RemoteException {
		initRoulette();
	}

	// Random number.
	public int number() throws RemoteException {
		Random rand = new Random();

		return rand.nextInt(36);
	}

	// Initialisation des encoche de la roulette.
	private void initRoulette() throws RemoteException {
		encoches.add(new Encoche(false, 32));
		encoches.add(new Encoche(true, 15));
		encoches.add(new Encoche(false, 19));
		encoches.add(new Encoche(true, 4));
		encoches.add(new Encoche(false, 21));
		encoches.add(new Encoche(true, 2));
		encoches.add(new Encoche(false, 25));
		encoches.add(new Encoche(true, 17));
		encoches.add(new Encoche(false, 34));
		encoches.add(new Encoche(true, 6));
		encoches.add(new Encoche(true, 27));
		encoches.add(new Encoche(false, 13));
		encoches.add(new Encoche(true, 36));
		encoches.add(new Encoche(false, 11));
		encoches.add(new Encoche(true, 30));
		encoches.add(new Encoche(false, 8));
		encoches.add(new Encoche(true, 23));
		encoches.add(new Encoche(false, 10));
		encoches.add(new Encoche(false, 5));
		encoches.add(new Encoche(true, 24));
		encoches.add(new Encoche(false, 26));
		encoches.add(new Encoche(true, 33));
		encoches.add(new Encoche(false, 1));
		encoches.add(new Encoche(true, 20));
		encoches.add(new Encoche(false, 14));
		encoches.add(new Encoche(true, 31));
		encoches.add(new Encoche(false, 9));
		encoches.add(new Encoche(true, 22));
		encoches.add(new Encoche(true, 18));
		encoches.add(new Encoche(false, 29));
		encoches.add(new Encoche(true, 7));
		encoches.add(new Encoche(false, 28));
		encoches.add(new Encoche(true, 12));
		encoches.add(new Encoche(false, 35));
		encoches.add(new Encoche(true, 3));
		encoches.add(new Encoche(false, 26));
	}

	public ArrayList<IEncoche> getEncoches() throws RemoteException {
		return encoches;
	}

	// M�thode de raffraichissement de la roulette.
	public void rafraichissementRoulette() throws RemoteException {
		ArrayList<IEncoche> rouletteInter = new ArrayList<IEncoche>();
		// Ajoute les chiffres � la roullette.
		for (int i = 0; i < encoches.size() - 1; i++) {
			rouletteInter.add(encoches.get(i + 1));
		}
		if (encoches.size() != 0) {
			rouletteInter.add(encoches.get(0));
			encoches = rouletteInter;
		}
	}

}
