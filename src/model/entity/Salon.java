package model.entity;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

/*
 * Classe Salon.
 */
@SuppressWarnings("serial")
public class Salon extends UnicastRemoteObject implements ISalon {

	// Attributs.
	private ArrayList<Joueur> players;
	private String nom;
	private IBille bille;
	private IRoulette roulette;
	private int nbPlayers;
	private Joueur master;
	private String masterName;
	private ArrayList<String> historique;
	private int nbMessage;

	// Constructeur.
	public Salon(Joueur player, String name) throws RemoteException {
		this.players = new ArrayList<Joueur>();
		this.players.add(player);
		this.nom = name;
		this.nbPlayers = 1;
		this.roulette = new Roulette();
		this.bille = new Bille();
		this.master = player;
		this.historique = new ArrayList<String>();
		this.nbMessage = 0;
		initHistorique();
	}

	public Salon(String name) throws RemoteException {
		this.players = new ArrayList<Joueur>();
		this.nom = name;
		this.nbPlayers = 0;
		this.roulette = new Roulette();
		this.bille = new Bille();
		System.out.println(name + " cr��");
		this.master = null;
		this.historique = new ArrayList<String>();
		this.nbMessage = 0;
		initHistorique();
	}

	// Initialisation de l'historique.
	private void initHistorique() {
		for (int i = 0; i < 20; i++) {
			this.historique.add("");
		}
	}

	// Getteur et setteur
	public ArrayList<Joueur> getPlayers() {
		return players;
	}

	public Joueur getMaster() throws RemoteException {
		return master;
	}

	public void setPlayers(ArrayList<Joueur> players) {
		this.players = players;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public IRoulette getRoulette() throws RemoteException {
		return roulette;
	}

	public void setRoulette(Roulette roulete) {
		this.roulette = roulete;
	}

	public int getNbPlayers() throws RemoteException {
		return nbPlayers;
	}

	public void setNbPlayers(int nbPlayers) {
		this.nbPlayers = nbPlayers;
	}

	public void setMaster() throws RemoteException {
		int i = 0;
		System.out.println("la liste des joueurs est : " + this.nbPlayers);
		while (i < this.nbPlayers && this.master == null) {
			if (this.players != null) {
				this.master = this.players.get(i);
				this.masterName = this.players.get(i).getName();
			}
		}
	}

	public String getMasterName() throws RemoteException {
		return this.masterName;
	}

	public void removeMaster() throws RemoteException {
		this.master = null;
	}

	public IBille getBille() {
		return this.bille;
	}

	public ArrayList<String> getHistorique() throws RemoteException {
		return historique;
	}

	// M�thode pour ajouter un joueur.
	public void addPlayer(Joueur player) throws RemoteException {
		players.add(player);
		System.out.println("joeur " + this.players.get(0).getName() + " id : " + masterName);
		this.nbPlayers++;
	}

	// Suppression d'un joueur dans la liste du salon.
	public void deletePlayer(Joueur pPlayer) throws RemoteException {
		for (int i = 0; i < players.size(); i++) {
			if (players.get(i).getName().equals(pPlayer.getName())) {
				players.remove(i);
				this.nbPlayers--;
				if (this.nbPlayers == 0) {
					this.getBille().setStopped(false);
				}
				System.out.println("Le joueur a delete a �t� trouv� |o| nom : " + pPlayer.getName()
						+ " nb player restants : " + this.nbPlayers);
			}
		}
	}

	// Affichage test pour voir les joueurs dans le salon.
	public String toStringAllJoueurs() throws RemoteException {
		String res = "";
		for (int i = 0; i < players.size(); i++) {
			res = res + players.get(i).getName() + " ";
		}
		return res;
	}

	// M�thode ajoutant un message.
	public void addMessage(String pMessage) throws RemoteException {
		this.historique.set((nbMessage % 20), pMessage);
		this.nbMessage++;
	}

}
