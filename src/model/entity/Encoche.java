package model.entity;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/*
 * Classe Encoche.
 */
@SuppressWarnings("serial")
public class Encoche extends UnicastRemoteObject implements IEncoche {
	// Attributs.
	private boolean color;
	private int number;

	// Constructeur.
	public Encoche(boolean color, int number) throws RemoteException {
		this.color = color;
		this.number = number;
	}

	// Getteur et setteurs.
	public boolean isColor() {
		return color;
	}

	public int getNumber() {
		return number;
	}
}
