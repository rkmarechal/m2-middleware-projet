package model.entity;

import java.io.Serializable;

/*
 * Classe PariElement.
 */
@SuppressWarnings("serial")
public class PariElement implements Serializable {
	private int numero;
	private int somme;

	// Constructeur de la classe.
	public PariElement(int numero) {
		this.numero = numero;
	}

	// Getteurs et setteurs.
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getSomme() {
		return somme;
	}

	public PariElement(int numero, int somme) {
		this.numero = numero;
		this.somme = somme;
	}

	public void setSomme(int somme) {
		this.somme = somme;
	}

}
