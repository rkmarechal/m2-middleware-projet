package model.entity;

import java.io.Serializable;
import java.rmi.RemoteException;

/*
 * Classe Joueur.
 */
@SuppressWarnings("serial")
public class Joueur implements Serializable {
	// Attributs
	private ISalon room;
	private String name;
	private int argent;
	private Pari p;
	private Boolean master;

	// Constructeur
	public Joueur(String pName, int pArgent) {
		this.setName(pName);
		this.setArgent(pArgent);
		this.p = new Pari();
		this.master = false;
	}

	// Getteur et Setteur
	public Pari getPari() {
		return p;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getArgent() {
		return argent;
	}

	public void setArgent(int argent) {
		this.argent = argent;
	}

	public ISalon getRoom() {
		return room;
	}

	public void setRoom(ISalon iSalon) {
		this.room = iSalon;
	}

	public Boolean getMaster() {
		return master;
	}

	// Informe le joueur qu'il est le master du salon.
	public void setMaster() throws RemoteException {
		if (this.getRoom() != null) {
			if (this.getRoom().getMasterName().equals(this.name)) {
				this.master = true;
			}
		}
	}

	// Fait quitter le joueur du salon et passe le master si le joueur l'�tait.
	public void deleteRoom() throws RemoteException {
		try {
			System.out.println("Joueurs restants :");
			this.room.toStringAllJoueurs();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Remplace le master.
		if (this.master) {
			this.room.removeMaster();
			this.room.setMaster();
			this.master = false;
			this.room.getBille().setStopped(false);
		}
		this.room = null;

	}
}
