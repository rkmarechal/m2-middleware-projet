package model.entity;

import java.rmi.Remote;
import java.rmi.RemoteException;

// Remote interface de l'encoche.
public interface IEncoche extends Remote {
	int getNumber() throws RemoteException;

	boolean isColor() throws RemoteException;
}
