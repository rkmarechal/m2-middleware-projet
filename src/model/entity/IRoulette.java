package model.entity;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

//Remote interface de la roulette. 
public interface IRoulette extends Remote {
	void rafraichissementRoulette() throws RemoteException;

	ArrayList<IEncoche> getEncoches() throws RemoteException;

	public int number() throws RemoteException;
}
