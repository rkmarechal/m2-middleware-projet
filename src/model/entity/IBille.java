package model.entity;

import java.rmi.Remote;
import java.rmi.RemoteException;

// Remote interface de la bille.
public interface IBille extends Remote {
	double getX() throws RemoteException;

	double getY() throws RemoteException;

	void setX(double x) throws RemoteException;

	void setY(double y) throws RemoteException;

	void resetbille() throws RemoteException;

	void setEncoche(IEncoche encoche) throws RemoteException;

	IEncoche getEncoche() throws RemoteException;

	Boolean getStopped() throws RemoteException;

	void setStopped(Boolean stopped) throws RemoteException;
}
