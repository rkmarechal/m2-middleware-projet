package model.entity;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

//Remote interface du salon. 
public interface ISalon extends Remote {

	String getNom() throws RemoteException;

	int getNbPlayers() throws RemoteException;

	void addPlayer(Joueur player) throws RemoteException;

	public void deletePlayer(Joueur pPlayer) throws RemoteException;

	public String toStringAllJoueurs() throws RemoteException;

	ArrayList<Joueur> getPlayers() throws RemoteException;

	public IBille getBille() throws RemoteException;

	public IRoulette getRoulette() throws RemoteException;

	public void setMaster() throws RemoteException;

	public Joueur getMaster() throws RemoteException;

	public String getMasterName() throws RemoteException;

	public void removeMaster() throws RemoteException;

	public void addMessage(String pMessage) throws RemoteException;

	public ArrayList<String> getHistorique() throws RemoteException;
}
