package model.entity;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Random;

/*
 * Classe Bille.
 */
@SuppressWarnings("serial")
public class Bille extends UnicastRemoteObject implements IBille {
	private double x;
	private double y;
	private IEncoche encocheCurrent;
	private Boolean jaja = false;

	// Constructeur
	public Bille(int x, int y) throws RemoteException {
		this.x = x;
		this.y = y;
	}

	// Constructeur
	public Bille() throws RemoteException {
		resetbille();
		this.setStopped(false);

		Random rand = new Random();
		double cX = 265.0;
		double cY = 265.0;
		double angle = Math.toRadians(1);
		int randomnumber = rand.nextInt(360);

		System.out.println("random " + randomnumber);
		for (int i = 0; i < randomnumber; i++) {
			this.x = (cX + (x - cX) * Math.cos(angle) - (y - cY) * Math.sin(angle));
			this.y = (cY + (x - cX) * Math.sin(angle) + (y - cY) * Math.cos(angle));
		}
	}

	// Getteurs et setteurs.
	public double getX() throws RemoteException {
		return this.x;
	}

	public void setX(double x) throws RemoteException {
		this.x = x;
	}

	public double getY() throws RemoteException {
		return y;
	}

	public void setY(double y) throws RemoteException {
		this.y = y;
	}

	public IEncoche getEncoche() throws RemoteException {
		return encocheCurrent;
	}

	public void setEncoche(IEncoche encoche) throws RemoteException {
		this.encocheCurrent = encoche;
	}

	public Boolean getStopped() throws RemoteException {
		return this.jaja;
	}

	public void setStopped(Boolean stopped) throws RemoteException {
		this.jaja = stopped;
	}

	// Reset la bille.
	public void resetbille() throws RemoteException {
		this.x = 285;
		this.y = 40;
	}

}
