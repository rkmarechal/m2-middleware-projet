package model.entity;

import java.io.Serializable;
import java.util.ArrayList;

/*
 * Classe Pari.
 */
@SuppressWarnings("serial")
public class Pari implements Serializable {
	// Attributs.
	private ArrayList<PariElement> tapis;

	// Constructeur de la classe.
	public Pari() {
		tapis = new ArrayList<PariElement>();
	}
	
	// Getteur pour les Paris.
	public ArrayList<PariElement> getPari() {
		return tapis;
	}

	// Ajout d'un chiffre dans le pari.
	public void addPari(PariElement pPariElement) {
		this.tapis.add(pPariElement);
	}

	// Suppression d'un chiffre dans le pari
	public void deletePari(int pInt) {
		for (int i = 0; i < tapis.size(); i++) {
			if (tapis.get(i).getNumero() == pInt) {
				tapis.remove(i);
			}
		}
	}

	// Affichage Pari
	public String toString() {
		String res = "";
		for (int i = 0; i < tapis.size(); i++) {
			res = res + "(" + tapis.get(i).getNumero() + ", " + tapis.get(i).getSomme() + ") ";
		}
		return res;
	}
}
