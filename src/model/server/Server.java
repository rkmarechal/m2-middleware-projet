package model.server;

import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

import model.game.GameRemote;
import model.game.IGame;

/*
 * Classe Server.
 */
public class Server extends Thread {
	// Constructeur de la classe.
	public Server() {
	}

	public void run() {
		try {
			// Initialisation de l'adresse du serveur.
			LocateRegistry.createRegistry(5000);
			IGame game;
			game = new GameRemote();
			Naming.bind("rmi://localhost:5000/game", game);
		} catch (MalformedURLException | AlreadyBoundException | RemoteException e) {
			e.printStackTrace();
		}
		System.out.println("Server ready");
	}

}
