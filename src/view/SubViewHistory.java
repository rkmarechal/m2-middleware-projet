package view;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;

/*
 * Classe de la vue pour l'historique de la partie.
 */
@SuppressWarnings("serial")
public class SubViewHistory extends JPanel {
	// Composant
	private GridLayout grid;
	private ArrayList<JLabel> chat;

	// Constructeur
	public SubViewHistory() {
		this.grid = new GridLayout(20, 1);
		this.chat = new ArrayList<JLabel>();
		for (int i = 0; i < 20; i++) {
			chat.add(new JLabel(""));
		}
	}

	public void init() {
		// Mise en place des composants.
		this.setLayout(grid);
		this.setBounds(650, 25, 275, 600);
		this.setBackground(new Color(255, 255, 255, 200));
		this.setOpaque(true);
		for (int i = 0; i < 20; i++) {
			this.add(chat.get(i));
		}
	}

	public void raffraichissement(ArrayList<String> pMessage) {
		for (int i = 0; i < pMessage.size(); i++) {
			this.chat.get(i).setText(pMessage.get(i));
		}
	}
}
