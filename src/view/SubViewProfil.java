package view;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;

/*
 * Classe de la vue du profil du joueur.
 */
@SuppressWarnings("serial")
public class SubViewProfil extends JPanel {

	// Composant du JPanel.
	private JLabel infoProfil;
	private JLabel infoName;
	private JLabel infoArgent;
	private JLabel name;
	private JLabel argent;
	private Font font;

	// Constructeur
	public SubViewProfil() {
		this.infoProfil = new JLabel("Profil".toUpperCase());
		this.infoName = new JLabel("Pseudo :");
		this.infoArgent = new JLabel("Argent disponible :");
		this.name = new JLabel();
		this.argent = new JLabel();
		this.font = new Font("Arial", Font.BOLD, 25);
	}

	public void init() {
		// Mise en place des composants.
		this.setLayout(null);
		this.setBounds(955, 25, 200, 100);
		this.setOpaque(true);
		this.setBackground(new Color(255, 255, 255, 200));
		this.infoProfil.setBounds(60, -80, 100, 200);
		this.infoProfil.setFont(font);
		this.infoName.setBounds(0, -40, 100, 200);
		this.name.setBounds(55, -40, 560, 200);
		this.infoArgent.setBounds(0, -20, 300, 200);
		this.argent.setBounds(110, -20, 500, 200);
		// Ajouts des �l�ments dans le JPanel.
		this.add(infoProfil);
		this.add(infoName);
		this.add(name);
		this.add(infoArgent);
		this.add(argent);
	}

	// Getteur et Setteur
	public void setTodoName(String pName) {
		name.setText(pName);
	}

	public void setArgent(int pArgent) {
		argent.setText(Integer.toString(pArgent));
	}

}
