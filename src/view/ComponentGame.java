package view;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JPanel;

/*
 * Classe de la vue de jeux.
 */
@SuppressWarnings("serial")
public class ComponentGame extends JPanel {
	public JButton getValidPari() {
		return validPari;
	}

	public void setValidPari(JButton validPari) {
		this.validPari = validPari;
	}

	// Composant du JPanel "ComponentGame".

	private SubViewProfil profil;
	private SubViewTableGame tableGame;
	private SubViewHistory history;
	private SubViewPlayer players;

	private JButton exitGame;

	public SubViewHistory getHistory() {
		return history;
	}

	public void setHistory(SubViewHistory history) {
		this.history = history;
	}

	public ConteneurRouletteBille getConteneurRouletteBille() {
		return conteneurRouletteBille;
	}

	public void setConteneurRouletteBille(ConteneurRouletteBille conteneurRouletteBille) {
		this.conteneurRouletteBille = conteneurRouletteBille;
	}

	private JButton validPari;
	private ConteneurRouletteBille conteneurRouletteBille = new ConteneurRouletteBille();

	// Constructeur
	public ComponentGame() {

		this.profil = new SubViewProfil();
		this.profil.init();
		this.tableGame = new SubViewTableGame();
		this.tableGame.init();
		this.history = new SubViewHistory();
		this.history.init();
		this.players = new SubViewPlayer();
		this.players.init();
		this.exitGame = new JButton("Retour");
		this.validPari = new JButton("Valider !");
	}

	@Override
	public void paintComponent(Graphics g) {

		// Chargement de l'image de fond.
		try {
			Image img = ImageIO.read(new File("src/view/images/background.png"));
			g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Désactivation de la mise en place auto.
		this.setLayout(null);

		// Réglage du JPanel (composant principal).
		this.setOpaque(false);
		this.exitGame.setLayout(null);
		this.validPari.setLayout(null);
		this.exitGame.setBounds(1000, 650, 150, 50);
		this.validPari.setBounds(1000, 575, 150, 50);
		// Ajout des JPanels.
		this.add(conteneurRouletteBille);
		this.add(profil);
		this.add(tableGame);
		this.add(history);
		this.add(players);
		this.add(exitGame);
		this.add(validPari);
	}

	public JButton getExitGame() {
		return exitGame;
	}

	public SubViewTableGame getTableGame() {
		return tableGame;
	}

	public SubViewProfil getSubViewProfil() {
		return profil;
	}

	public SubViewPlayer getSubViewPlayer() {
		return players;
	}

}