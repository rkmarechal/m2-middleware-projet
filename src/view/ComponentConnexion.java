package view;

import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/*
 * Classe de la vue de connexion
 */
@SuppressWarnings("serial")
public class ComponentConnexion extends JPanel {
	// Composant du JPanel "ComponentConnexion".
	private JLabel infoMain = new JLabel();
	private JLabel infoName = new JLabel();
	private JTextField inName = new JTextField();
	private JLabel infoArgent = new JLabel();
	private Integer[] elem = new Integer[] { 10, 100, 500, 1000, 5000, 10000 };
	private JComboBox<Integer> chooseMise = new JComboBox<Integer>(elem);
	private JButton valid = new JButton();

	// Constructeur de la vue "ComponentConnexion".
	public ComponentConnexion() {
		// Mise en place des composants

		// D�sactivation de la mise en place auto
		this.setLayout(null);

		// R�glage du JPanel (composant principal)
		this.setBackground(Color.LIGHT_GRAY);

		// R�glage des sous-composants

		// Premi�re ligne
		infoMain.setText("Information de connexion");
		infoMain.setBounds(600, 200, 150, 50);
		// Seconde Ligne.
		infoName.setText("Pseudo :");
		infoName.setBounds(500, 275, 150, 50);
		inName.setBounds(600, 290, 200, 25);
		// Troisieme ligne.
		infoArgent.setText("Argent :");
		infoArgent.setBounds(500, 305, 150, 50);
		chooseMise.setBounds(600, 320, 50, 25);
		// Quatri�me ligne.
		valid.setBounds(600, 350, 100, 25);
		valid.setText("Confirmer !");

		// Ajout des boutons
		this.add(infoMain);
		this.add(infoName);
		this.add(inName);
		this.add(infoArgent);
		this.add(chooseMise);
		this.add(valid);

		this.validate();
	}

	// Getteur.
	public JLabel getInfoMain() {
		return infoMain;
	}

	public JTextField getInName() {
		return inName;
	}

	public JComboBox<Integer> getChooseMise() {
		return chooseMise;
	}

	public JLabel getInfoName() {
		return infoName;
	}

	public JLabel getInfoArgent() {
		return infoArgent;
	}

	public JButton getValid() {
		return valid;
	}

}
