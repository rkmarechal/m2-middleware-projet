package view;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/*
 * Classe des bouttons de la vue d'accueil.
 */
@SuppressWarnings("serial")
public class SubViewMenuConfig extends JPanel {
	// Composant du JPanel subViewMenuConfig.
	// private JButton join;
	private JButton create;
	private JButton connexion;
	private JButton exit;
	private JLabel verifConnexion;

	// Constructeur.
	public SubViewMenuConfig() {
		this.create = new JButton("Cr�er Salon");
		this.connexion = new JButton("Mise � jours des salons");
		this.exit = new JButton("Quitter");
		this.verifConnexion = new JLabel();
	}

	// Initialisation de la vue.
	public void init() {
		// Mise en place des composants.
		this.setLayout(null);
		this.setBounds(650, 175, 1000, 1000); // x, y, largeur, longueur
		this.setBackground(Color.WHITE);
		this.setOpaque(false);
	}

	// M�thode d'affichage du JPanel.
	@Override
	public void paintComponent(Graphics g) {
		this.create.setLayout(null);
		this.connexion.setLayout(null);
		this.exit.setLayout(null);
		// R�glage des sous-composants.
		this.verifConnexion.setBounds((this.getWidth() / 4) + this.connexion.getWidth() / 2 + 25,
				(this.getHeight() / 3) + 60, 150, 50);
		this.verifConnexion.setForeground(Color.GREEN);
		this.create.setBounds((this.getWidth() / 4) + this.connexion.getWidth() / 2, (this.getHeight() / 3) + 25, 150,
				50);
		this.connexion.setBounds((this.getWidth() / 4) + this.connexion.getWidth() / 2, (this.getHeight() / 3) + 100,
				150, 50);
		this.exit.setBounds((this.getWidth() / 4) + this.connexion.getWidth() / 2, (this.getHeight() / 3) + 175, 150,
				50);
		this.add(create);
		this.add(connexion);
		this.add(exit);
		this.add(verifConnexion);
	}

	// Getteur des sous-composants.
	public JButton getConnexion() {
		return connexion;
	}

	public JButton getExit() {
		return exit;
	}

	public JButton getCreate() {
		return create;
	}

	public JLabel getVerifConnexion() {
		return this.verifConnexion;
	}
}
