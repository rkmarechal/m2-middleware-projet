package view;

import javax.swing.JFrame;
import javax.swing.JPanel;

/*
 * Classe de la frame (conteneur des JPanels).
 */
@SuppressWarnings("serial")
public class View extends JFrame {

	// D�claration des principaux JPanels.
	private ComponentAccueil cA;
	private ComponentConnexion cC;
	private ComponentGame cG;

	// D�claration des paramétres de la fenétre.
	private int height;
	private int width;

	// Constructeur de la vue principale (JFrame).
	public View() {
		// Instanciation + initialisation des JPanels.
		this.cA = new ComponentAccueil();
		this.cC = new ComponentConnexion();
		this.cG = new ComponentGame();
		this.height = 1200;
		this.width = 800;
	}

	// Initialisation de la vue.
	public void init() {
		this.setTitle("Application CASINO");
		this.setLocationRelativeTo(null);
		this.setBounds(100, 100, height, width);
		this.setContentPane(cC);
		this.setResizable(true);
		this.setVisible(true);
	}

	// Getteur pour accéder au JPanel.
	public ComponentAccueil getComponentAccueil() {
		return this.cA;
	}

	public ComponentConnexion getComponentConnexion() {
		return this.cC;
	}

	public ComponentGame getComponentGame() {
		return this.cG;
	}

	// M�thode pour changer de contexte.
	public void changementContexte(JPanel pF) {
		this.setContentPane(pF);
		this.validate();
	}

	public void changementContexte(ComponentAccueil componentAccueil) {
		// TODO Auto-generated method stub
		this.setContentPane(componentAccueil);
		this.validate();
	}
}
