package view;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

/*
 * Classe de la vue pour latable de jeux.
 */
@SuppressWarnings("serial")
public class SubViewTableGame extends JPanel {

	// Composant = Grille pour la table.
	GridLayout grid;
	JButton case1;
	JButton case2;
	JButton case3;
	JButton case4;
	JButton case5;
	JButton case6;
	JButton case7;
	JButton case8;
	JButton case9;
	JButton case10;
	JButton case11;
	JButton case12;
	JButton case13;
	JButton case14;
	JButton case15;
	JButton case16;
	JButton case17;
	JButton case18;
	JButton case19;
	JButton case20;
	JButton case21;
	JButton case22;
	JButton case23;
	JButton case24;
	JButton case25;
	JButton case26;
	JButton case27;
	JButton case28;
	JButton case29;
	JButton case30;
	JButton case31;
	JButton case32;
	JButton case33;
	JButton case34;
	JButton case35;
	JButton case36;

	// Constructeur
	public SubViewTableGame() {
		this.grid = new GridLayout(12, 3);
		this.case1 = new JButton("1");
		this.case2 = new JButton("2");
		this.case3 = new JButton("3");
		this.case4 = new JButton("4");
		this.case5 = new JButton("5");
		this.case6 = new JButton("6");
		this.case7 = new JButton("7");
		this.case8 = new JButton("8");
		this.case9 = new JButton("9");
		this.case10 = new JButton("10");
		this.case11 = new JButton("11");
		this.case12 = new JButton("12");
		this.case13 = new JButton("13");
		this.case14 = new JButton("14");
		this.case15 = new JButton("15");
		this.case16 = new JButton("16");
		this.case17 = new JButton("17");
		this.case18 = new JButton("18");
		this.case19 = new JButton("19");
		this.case20 = new JButton("20");
		this.case21 = new JButton("21");
		this.case22 = new JButton("22");
		this.case23 = new JButton("23");
		this.case24 = new JButton("24");
		this.case25 = new JButton("25");
		this.case26 = new JButton("26");
		this.case27 = new JButton("27");
		this.case28 = new JButton("28");
		this.case29 = new JButton("29");
		this.case30 = new JButton("30");
		this.case31 = new JButton("31");
		this.case32 = new JButton("32");
		this.case33 = new JButton("33");
		this.case34 = new JButton("34");
		this.case35 = new JButton("35");
		this.case36 = new JButton("36");

	}

	// M�thode d'initialisation
	public void init() {
		this.setLayout(grid);
		this.setBounds(955, 150, 200, 400);

		// Case pour parier.
		this.add(case1);
		this.add(case2);
		this.add(case3);
		this.add(case4);
		this.add(case5);
		this.add(case6);
		this.add(case7);
		this.add(case8);
		this.add(case9);
		this.add(case10);
		this.add(case11);
		this.add(case12);
		this.add(case13);
		this.add(case14);
		this.add(case15);
		this.add(case16);
		this.add(case17);
		this.add(case18);
		this.add(case19);
		this.add(case20);
		this.add(case21);
		this.add(case22);
		this.add(case23);
		this.add(case24);
		this.add(case25);
		this.add(case26);
		this.add(case27);
		this.add(case28);
		this.add(case29);
		this.add(case30);
		this.add(case31);
		this.add(case32);
		this.add(case33);
		this.add(case34);
		this.add(case35);
		this.add(case36);
	}

	// Getteur
	public JButton getCase1() {
		return case1;
	}

	public JButton getCase2() {
		return case2;
	}

	public JButton getCase3() {
		return case3;
	}

	public JButton getCase4() {
		return case4;
	}

	public JButton getCase5() {
		return case5;
	}

	public JButton getCase6() {
		return case6;
	}

	public JButton getCase7() {
		return case7;
	}

	public JButton getCase8() {
		return case8;
	}

	public JButton getCase9() {
		return case9;
	}

	public JButton getCase10() {
		return case10;
	}

	public JButton getCase11() {
		return case11;
	}

	public JButton getCase12() {
		return case12;
	}

	public JButton getCase13() {
		return case13;
	}

	public JButton getCase14() {
		return case14;
	}

	public JButton getCase15() {
		return case15;
	}

	public JButton getCase16() {
		return case16;
	}

	public JButton getCase17() {
		return case17;
	}

	public JButton getCase18() {
		return case18;
	}

	public JButton getCase19() {
		return case19;
	}

	public JButton getCase20() {
		return case20;
	}

	public JButton getCase21() {
		return case21;
	}

	public JButton getCase22() {
		return case22;
	}

	public JButton getCase23() {
		return case23;
	}

	public JButton getCase24() {
		return case24;
	}

	public JButton getCase25() {
		return case25;
	}

	public JButton getCase26() {
		return case26;
	}

	public JButton getCase27() {
		return case27;
	}

	public JButton getCase28() {
		return case28;
	}

	public JButton getCase29() {
		return case29;
	}

	public JButton getCase30() {
		return case30;
	}

	public JButton getCase31() {
		return case31;
	}

	public JButton getCase32() {
		return case32;
	}

	public JButton getCase33() {
		return case33;
	}

	public JButton getCase34() {
		return case34;
	}

	public JButton getCase35() {
		return case35;
	}

	public JButton getCase36() {
		return case36;
	}

}
