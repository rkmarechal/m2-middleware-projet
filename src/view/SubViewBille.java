package view;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class SubViewBille extends JPanel {
	// Composant du Jpanel.
	private int xc;
	private int yc;

	// Constructeur
	public SubViewBille() {
		this.xc = 285;
		this.yc = 40;
	}

	// M�thode d'initialisation
	public void init() {
		this.setLayout(null);
		this.setBounds(20, 20, 580, 580);
		this.setOpaque(false);
	}

	public int getXc() {
		return xc;
	}

	public int getYc() {
		return yc;
	}

	// M�thode d'affichage du JPanel (Affiche la roulette).
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(Color.BLACK);
		g.fillOval(xc, yc, 25, 25);
	}

	public void rafraichissement(int x, int y) {
		this.xc = x;
		this.yc = y;
		this.revalidate();
		this.repaint();
	}
	/* fin de paintComponent */
}
