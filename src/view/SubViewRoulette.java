package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.JPanel;

import model.entity.IEncoche;

/*
 * Classe de la vue pour la roulette.
 */
@SuppressWarnings("serial")
public class SubViewRoulette extends JPanel {

	// Composant du Jpanel.
	private int xc;
	private int yc;
	private Font font;
	private int tableauEntier[] = { 32, 15, 19, 4, 21, 2, 25, 17, 34, 6, 27, 13, 36, 11, 30, 8, 23, 10, 5, 24, 26, 33,
			1, 20, 14, 31, 9, 22, 18, 29, 7, 28, 12, 35, 3, 26 };
	private int tableauEntierPosX[] = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };
	private int tableauEntierPosY[] = { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
			-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };

	public int[] getTableauEntierPosX() {
		return tableauEntierPosX;
	}

	public int[] getTableauEntierPosY() {
		return tableauEntierPosY;
	}

	private boolean colorCase[] = { true, false, true, false, true, false, true, false, true, false, true, false, true,
			false, true, false, true, false, true, false, true, false, true, false, true, false, true, false, true,
			false, true, false, true, false, true, false };

	// Constructeur.
	public SubViewRoulette() {
	}

	// M�thode d'initialisation.
	public void init() {
		this.setLayout(null);
		this.setBounds(0, 0, 600, 600);
		this.setOpaque(false);
	}

	// M�thode d'affichage du JPanel (Affiche la roulette).
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		xc = getWidth() / 2;
		yc = getHeight() / 2;
		int rayon = Math.min(xc, yc) * 92 / 100;

		font = new Font("Times New Roman", 0, 15);
		g.setFont(font);

		g.setColor(new Color(255, 255, 255, 250));
		g.fillOval(xc - 300, yc - 300, 600, 600);
		g.setColor(Color.black);
		g.drawOval(xc - 300, yc - 300, 600, 600);

		g.setColor(new Color(0, 128, 0, 150));
		g.fillOval(xc - 250, yc - 250, 500, 500);
		g.setColor(Color.black);
		g.drawOval(xc - 250, yc - 250, 500, 500);

		g.setColor(new Color(255, 255, 0, 200));
		g.fillOval(xc - 40, yc - 40, 80, 80);
		g.setColor(Color.black);
		g.drawOval(xc - 40, yc - 40, 80, 80);

		for (int i = 0; i <= 35; i++) {
			if (colorCase[i] == true) {
				g.setColor(Color.red);
			} else {
				g.setColor(Color.black);
			}
			double angle = i * Math.PI / 18.0 - Math.PI / 2.0;
			double x = xc + rayon * Math.cos(angle);
			double y = yc + rayon * Math.sin(angle);
			tableauEntierPosX[i] = (int) x;
			tableauEntierPosY[i] = (int) y;
			g.drawString(" " + tableauEntier[i], (int) x, (int) y);
			g.setColor(Color.black);
			int xsf = xc + (int) (1.09 * rayon * Math.cos(angle));
			int ysf = yc + (int) (1.09 * rayon * Math.sin(angle));
			g.drawLine(xc, yc, xsf, ysf);
		}

	}/* fin de paintComponent */

	public void rafraichissement(ArrayList<IEncoche> encoche) throws RemoteException {
		if (encoche.size() != 0) {
			for (int i = 0; i < (tableauEntier.length); i++) {
				tableauEntier[i] = encoche.get(i).getNumber();
				colorCase[i] = encoche.get(i).isColor();
			}
			this.repaint();
		}
	}
}