package view.audio;

import java.applet.Applet;
import java.applet.AudioClip;

public class Sound extends Thread{

	public static final Sound[] sounds = { new Sound("music.wav") };

	private AudioClip clip;

	private Sound(String name) {
		clip = Applet.newAudioClip(getClass().getClassLoader().getResource(name));
	}
	
	public void run() {
		clip.play();
	}
	
	public void stopa() {
		clip.stop();
		this.interrupt();
	}
}