package view;

import javax.swing.JLayeredPane;

/*
 * Classe ConteneurRouletteBille.
 */
@SuppressWarnings("serial")
public class ConteneurRouletteBille extends JLayeredPane {
	// Composant du JPanel.
	private SubViewRoulette roulette;
	private SubViewBille bille;

	// Constructeur
	public ConteneurRouletteBille() {
		this.setLayout(null);
		this.setOpaque(false);
		this.setBounds(20, 20, 600, 600);
		this.roulette = new SubViewRoulette();
		this.roulette.init();
		this.bille = new SubViewBille();
		bille.init();
		this.add(roulette, new Integer(1));
		this.add(bille, new Integer(2));
	}

	// Getteur et Setteur.
	public SubViewBille getBille() {
		return bille;
	}

	public void setBille(SubViewBille bille) {
		this.bille = bille;
	}

	public SubViewRoulette getSubViewMenuConfig() {
		return roulette;
	}
	
	public SubViewRoulette getRoulette() {
		return roulette;
	}

	public void setRoulette(SubViewRoulette roulette) {
		this.roulette = roulette;
	}
}
