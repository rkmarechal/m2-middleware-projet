package view;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;

import model.entity.Joueur;

/*
 * Classe de la vue des joueurs dans le salon.
 */
@SuppressWarnings("serial")
public class SubViewPlayer extends JPanel {
	// Composant du JPanel.
	GridLayout grid;
	JLabel nameJoueur1;
	JLabel nameJoueur2;
	JLabel nameJoueur3;
	JLabel nameJoueur4;

	// Constructeur
	public SubViewPlayer() {
		this.grid = new GridLayout(1, 4);
		this.nameJoueur1 = new JLabel("NULL");
		this.nameJoueur2 = new JLabel("NULL");
		this.nameJoueur3 = new JLabel("NULL");
		this.nameJoueur4 = new JLabel("NULL");
	}

	// M�thode d'initialisation du JPanel.
	public void init() {
		// Mise en place des composants.
		this.setLayout(grid);
		this.setBounds(25, 625, 600, 100);
		this.setBackground(new Color(255, 255, 255, 200));
		this.setOpaque(true);
		this.add(nameJoueur1);
		this.add(nameJoueur2);
		this.add(nameJoueur3);
		this.add(nameJoueur4);
		this.validate();
	}

	public void raffraichissement(ArrayList<Joueur> players, int nbPlayers) {
		// System.out.println("coucou");
		for (int i = 0; i < nbPlayers; i++) {
			if (i == 0)
				setNameJoueur1(players.get(0).getName());
			if (i == 1)
				setNameJoueur2(players.get(1).getName());
			if (i == 2)
				setNameJoueur3(players.get(2).getName());
			if (i == 3)
				setNameJoueur4(players.get(3).getName());
		}
		for (int i = nbPlayers; i < 5; i++) {
			if (i == 0)
				setNameJoueur1("NULL");
			if (i == 1)
				setNameJoueur2("NULL");
			if (i == 2)
				setNameJoueur3("NULL");
			if (i == 3)
				setNameJoueur4("NULL");
		}
	}

	// Getteur et Setteur.
	public JLabel getNameJoueur1() {
		return nameJoueur1;
	}

	public void setNameJoueur1(String pNameJoueur1) {
		this.nameJoueur1.setText(pNameJoueur1);
	}

	public JLabel getNameJoueur2() {
		return nameJoueur2;
	}

	public void setNameJoueur2(String pNameJoueur2) {
		this.nameJoueur2.setText(pNameJoueur2);
	}

	public JLabel getNameJoueur3() {
		return nameJoueur3;
	}

	public void setNameJoueur3(String pNameJoueur3) {
		this.nameJoueur3.setText(pNameJoueur3);
	}

	public JLabel getNameJoueur4() {
		return nameJoueur4;
	}

	public void setNameJoueur4(String pNameJoueur4) {
		this.nameJoueur4.setText(pNameJoueur4);
	}
}
