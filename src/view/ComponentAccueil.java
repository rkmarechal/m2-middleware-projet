package view;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

/*
 * Classe de la vue d'accueil.
 */
@SuppressWarnings("serial")
public class ComponentAccueil extends JPanel {
	// Composant du JPanel "ComponantAccueil".
	private SubViewMenuConfig menuConfig;
	private SubViewProfil menuJoueur;
	private SubViewGame menuGame;

	// Constructeur de la vue "ComponentAccueil".
	public ComponentAccueil() {
		this.menuConfig = new SubViewMenuConfig();
		this.menuConfig.init();
		this.menuJoueur = new SubViewProfil();
		this.menuJoueur.init();
		this.menuGame = new SubViewGame();
		this.menuGame.init();
	}

	// M�thode d'afichage des �l�ments du JPanel.
	@Override
	public void paintComponent(Graphics g) {

		// Chargement de l'image de fond.
		try {
			Image img = ImageIO.read(new File("src/view/images/background.png"));
			g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// D�sactivation de la mise en place auto.
		this.setLayout(null);

		// R�glage du JPanel (composant principal).
		this.setOpaque(false);

		// Ajout des JPanels.
		this.add(menuConfig);
		this.add(menuJoueur);
		this.add(menuGame);
	}

	// Getteur.
	public SubViewMenuConfig getSubViewMenuConfig() {
		return menuConfig;
	}

	public SubViewProfil getSubViewProfil() {
		return menuJoueur;
	}

	public SubViewGame getSubViewGame() {
		return menuGame;
	}
}
