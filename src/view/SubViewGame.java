package view;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

/*
 * Classe de la vue d'affichage des salons.
 */
@SuppressWarnings("serial")
public class SubViewGame extends JPanel {
	// Composant du JPanel.
	GridLayout grid;
	JButton salon1;
	JButton salon2;
	JButton salon3;
	JButton salon4;
	JButton salon5;

	// Constructeur
	public SubViewGame() {
		this.grid = new GridLayout(5, 1);
	}

	public void init() {
		// Mise en place des composants.
		this.setLayout(grid);
		this.setBounds(20, 20, 800, 700);
		this.setOpaque(true);
		this.setBackground(new Color(255, 255, 255, 200));
	}

	// M�thode d'initialisation.
	public void initsalon(String nom, int roomnumber) {
		JButton salon = new JButton(nom);
		salon = new JButton();
		if (roomnumber == 0) {
			this.salon1 = salon;
		} else if (roomnumber == 1) {
			this.salon2 = salon;
		} else if (roomnumber == 2) {
			this.salon3 = salon;
		} else if (roomnumber == 3) {
			this.salon4 = salon;
		} else if (roomnumber == 4) {
			this.salon5 = salon;
		}
	}

	// Getteur et Setteur.
	public JButton getSalon2() {
		return salon2;
	}

	public void setSalon2(JButton salon2) {
		this.salon2 = salon2;
	}

	public JButton getSalon3() {
		return salon3;
	}

	public void setSalon3(JButton salon3) {
		this.salon3 = salon3;
	}

	public JButton getSalon4() {
		return salon4;
	}

	public void setSalon4(JButton salon4) {
		this.salon4 = salon4;
	}

	public JButton getSalon5() {
		return salon5;
	}

	public void setSalon5(JButton salon5) {
		this.salon5 = salon5;
	}

	public JButton getSalon1() {
		return salon1;
	}

	public void setSalon1(JButton p_button) {
		this.salon1 = p_button;
	}
}
