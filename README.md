# M2 middleware

## Installation de l'environnement

Cloner le projet git et importer le projet dans eclipse en tant que `Java Project`.

## Lancement de l'application

Une fois l'installation terminée, lancer une compilation `Java Application` en faisant un clique droit sur le fichier `client.java` contenant le main de l'application.